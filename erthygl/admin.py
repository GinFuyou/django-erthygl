from django.contrib import admin
from django.db.models import F

from django_summernote.widgets import SummernoteWidget
from .models import Article, ArticleCategory
from django import forms
from django.utils.html import format_html, mark_safe
from django.apps import apps
from django.template import Context, loader
from django.conf import settings



class ArticleAdminForm(forms.ModelForm):
    class Meta:
        model = Article
        widgets = {
            'html': SummernoteWidget(attrs={'cols': 64, 'rows': 12}),
            'meta_descr': forms.Textarea(attrs={'cols': 66, 'rows': 3}),
            'meta_extra': forms.Textarea(attrs={'cols': 66, 'rows': 3}),
            'intro': forms.Textarea(attrs={'cols': 64, 'rows': 2}),
            'extra_settings': forms.Textarea(attrs={'cols': 64, 'rows': 2}),
            'meta_keywords': forms.TextInput(attrs={'size':68}),
            'article_title': forms.TextInput(attrs={'size':80}),    }
        fields = '__all__'



@admin.register(Article)
class ArticleAdo(admin.ModelAdmin):
    form = ArticleAdminForm

    def save_model(self, request, obj, form, change):
        obj.creator = request.user
        super().save_model(request, obj, form, change)

    class Media:
        js = ('admin/erthygl_admin.js', settings.FONTAWESOME_JS_CDN[0])
        css = {'all': ('main.css', )}


    def imagepicker(self, obj=None):
        Model = apps.get_model(settings.ERTHYGL_IMAGEMODEL)
        qs = Model._default_manager.all()
        ordering = getattr(settings, 'ERTHYGL_IMAGEORDER', None)
        if ordering:
            qs = qs.order_by(ordering)

        template = loader.get_template("erthygl.imagepicker.html")
        con = {"images": qs, 'size': '160x120'}
        html = mark_safe(template.render(con))
        return html

    def seo_check(self, obj):
        if obj.meta_index:
            flag = len(obj.meta_title)>4 and len(obj.meta_keywords)>5 and len(obj.meta_descr)>7
        else:
            flag = False
            return mark_safe('<i class="fa fa-ban red" aria-hidden="true" title="индексирование запрещено"></i>')

        if flag:
            return mark_safe('<i class="fa fa-check-square clr_green" aria-hidden="true" title="все meta заданы, индексирование разрешено"></i>')
        else:
            return mark_safe('<i class="fa fa-exclamation-triangle red" aria-hidden="true" title="какой-то из meta слишком короткий"></i>')

    seo_check.allow_tags = True
    seo_check.short_description = 'SEO'

    def toggle_public(self, request, queryset):
        pub, depub = 0, 0
        for a in queryset:
            a.public = not a.public
            if a.public:
                pub += 1
            else:
                depub += 1
            a.save()  # This is needed to trigger save custom logic
        #c = queryset.update(public=not F('public'))
        self.message_user(request, 'Страниц опубликовано {0}, страниц снято с публикации: {1}'.format(pub, depub))
    toggle_public.short_description = 'Сменить статус публикации'

    def descr_charcount(self, obj):
        count = len(obj.meta_descr)
        return count
    descr_charcount.short_description = 'Символов'

    actions = ['toggle_public',]
    list_display = (
        'slug', 'revision', 'public','seo_check', 'category',
        'create_dt', 'update_dt', 'get_absolute_url')
    search_fields = ('slug', 'meta_title')
    list_filter = ('public', 'category', 'creator',)
    readonly_fields =(
        'descr_charcount', 'html_hash', 'seo_check', 'imagepicker',
         'safe_html', 'update_dt', 'get_absolute_url', 'creator')
    fieldsets = (
        ('Base', {'fields': (('meta_title', 'meta_index', 'text_index'), 'meta_keywords',
                    ('meta_descr', 'descr_charcount'),
                    ('meta_extra', 'get_absolute_url'),
                    ('slug', 'public', ), 
                    ('category', 'sitemap_priority'),
                    ('head_image', 'extra_settings'),
                    'intro', 'article_title', 'html',
                    ('html_hash', 'revision', 'store_revisions') ),    }
                 ),
        ('Изображения',
            {   'fields': ( 'imagepicker', ),
                'classes': ('collapse',)
            },
            ),
        ('Дополнительно',
            {   'fields': 
                (
                 ('creator', 'create_dt', 'update_dt'), 'safe_html', # rendered not implemented
                ),
                'classes': ('collapse',)
            },
            ),
        )

@admin.register(ArticleCategory)
class ArticleCategoryAdo(admin.ModelAdmin):
    list_display = ('title', 'slug', 'id')
