django.jQuery(document).ready(
function()
{
    django.jQuery('#id_meta_descr').on('input', 
    function()
    {
        var count;
        count = django.jQuery('#id_meta_descr').val().length;
        django.jQuery('.field-descr_charcount').find('.readonly').text(count);
    });

    django.jQuery('#id_html_iframe').on("load",
    function() 
    {
        setTimeout(function () { django.jQuery('#id_html_iframe').contents().find('.note-editable').addClass('central'); }, 3500);
    });

});
