from django.conf import settings
from django.apps import apps
from django.template import Context, loader
from django.utils.html import mark_safe


def make_imagepicker():
    Model = apps.get_model(settings.ERTHYGL_IMAGEMODEL)
    qs = Model._default_manager.all()

    template = loader.get_template("erthygl.imagepicker.html")
    con = Context({"images": qs})
    html = mark_safe(ten.render(con))
    return html
