import os
import re
from hashlib import sha1

import bleach

from  django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.contrib.postgres.fields import JSONField
#from django.core.files import File
from django.db import models, transaction
from django.utils import timezone, html
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.template import Context, Template
from django.conf import settings
from django.apps import apps



def repl_image(matchobj):
    img_id = int(matchobj.group('id'))
    Model = apps.get_model(settings.ERTHYGL_IMAGEMODEL)
    try:
        i = Model.objects.get(id=img_id)
        alias = i.img[settings.ERTHYGL_DEFAULT_ALIAS]
        h = '<a class="magnific" href="{i.img.url}" target="_blank"><img src="{alias.url}" alt="{i.title}"' \
            ' class="article_img" style=" width: {alias.width}px; height: {alias.height}px;"/></a>'
        h = h.format(alias=alias, i=i)
        h = html.mark_safe(h)
    except Model.DoesNotExist:
        return "<!- image does not exists -->"
    except Exception as ex:
        return "<!- image not loaded -->"
    else:
        return h


class ArticleCategory(models.Model):
    class Meta:
        verbose_name = "Категория"
        db_table = 'erthygl_categories'
        verbose_name_plural = "Категории"

    slug = models.SlugField(max_length=48, unique=True)
    title = models.CharField(max_length=256, unique=True)
    def __str__(self):
        return "{0.title}".format(self)


class Article(models.Model):
    class Meta:
        verbose_name = "Статья"
        db_table = 'erthygl_articles'
        verbose_name_plural = "Статьи"
        unique_together = ('slug', 'revision')

    _revisioning_fields = ('html_hash', 'intro', 'meta_title', 'meta_keywords', 'meta_descr')  # fields compared for revision control
    _allowed_tags = [
        'p', 'a', 'i', 'b', 'li', 'ol', 'ul','section', 'div',
        'blockquote', 'code', 'hr', 'table', 'tbody', 'caption',
        'tr', 'td', 'th', 'h2', 'h3', 'h4', 'h5', 'h6', 'img',
        'br', 'sup', 'sub']
    _allowed_attrs = {
        '*': ['class', 'id', 'aria-hidden'],
        'a': ['href', 'rel', 'title'],
        'div': ['style',],
        'i': ['style', 'data-fa-transform'],
        'span': ['style', 'data-fa-transform'],
        'img': ['alt', 'title', 'src', 'style'], }
    _allowed_styles = [
        'color', 'font-weight', 'font-size',
        'margin', 'padding', 'float', 'width', 'height',
        'max-width', 'max-height', 'text-align', 'float']

    meta_title = models.CharField(max_length=756, blank=True)
    meta_keywords = models.CharField(max_length=756, blank=True)
    meta_descr = models.TextField(max_length=4096, blank=True)
    meta_index = models.BooleanField(default=True, help_text='Разрешить индексирование в meta robots')  # WARNING
    text_index = models.BooleanField(default=True, help_text='Разрешить индексирование текста статьи (иначе обернуть в &lt;!--noindex--&gt;)')
    meta_extra = models.TextField(default='', blank=True)

    slug = models.SlugField(max_length=256)
    category = models.ForeignKey(ArticleCategory, on_delete=models.PROTECT)

    intro = models.TextField(blank=True, help_text='Короткий вводный текст для отображения в списке статей')
    article_title = models.CharField(max_length=384, blank=True, default='')
    head_image = models.ForeignKey(settings.ERTHYGL_IMAGEMODEL,
                                   blank=True,
                                   null=True,
                                   on_delete=models.SET_NULL)
    html = models.TextField(blank=True)
    html.help_text='Текст статьи (HTML) Чтобы вставить картинку, скопируйте тег {{ aimg_XX }}' \
        ' из списка ниже, вставьте в нужное место статьи и сохраните её.'
    html_hash = models.CharField(max_length=128, blank=True, default="")
    rendered = models.TextField(blank=True, editable=False)

    public = models.BooleanField(
        default=False,
        help_text='Опубликовать статью (все предыдущие ревизии будут сняты с публикации)')

    sitemap_priority = models.PositiveSmallIntegerField(
        default=5, validators=[MinValueValidator(1), MaxValueValidator(10)],
        help_text='приоритет для sitemap от 1 до 10 (будет поделен на 10)')

    create_dt = models.DateTimeField(default=timezone.localtime)
    update_dt = models.DateTimeField(auto_now=True)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                blank=True, 
                                null=True,
                                on_delete=models.SET_NULL)

    store_revisions = models.BooleanField(
        default=getattr(settings, 'ERTHYGL_REVISIONS', True),
        help_text='сохранять ревизию при редактировании')
    revision = models.PositiveSmallIntegerField(default=0)

    char_count = models.PositiveIntegerField(default=0, editable=False)
    extra_settings = JSONField(blank=True, default=getattr(settings, 'ERTHYGL_DEFAULT_EXTRA', ''))
    extra_settings.help_text = 'В формате JSON, "" для пустого значения'

    def __str__(self):
        return "{0} (rev.{1})".format(self.slug, self.revision)

    def make_hash(self):
        if self.html:
            return sha1(self.html.strip().encode('utf-8')).hexdigest()
        else:
            return ""

    def get_absolute_url(self):
        if not self.pk:
            return ''
        try:
            return reverse(settings.ERTHYGL_ARTICLE_VIEW_NAME, kwargs={'slug': self.slug})
        except NoReverseMatch:
            return '/no-reverse-match/'

    def bleach_html(self):
        allowed_tags = getattr(settings, 'ERTHYGL_TAGS', self._allowed_tags)
        allowed_attrs = getattr(settings, 'ERTHYGL_ATTRS', self._allowed_attrs)
        allowed_styles = getattr(settings, 'ERTHYGL_STYLES', self._allowed_styles)
        clean_html = bleach.clean(
            self.html, 
            tags=allowed_tags,
            attributes=allowed_attrs,
            styles=allowed_styles)
        return clean_html

    def save(self, *args, **kwargs):
        self.html = self.preprocess_images()
        self.char_count = len(self.html)
        self.html_hash = self.make_hash()

        if self.store_revisions and self.pk:
            old_obj = Article.objects.get(pk=self.pk)
            change_flag = False
            for fieldname in self._revisioning_fields:
                if getattr(self, fieldname) != getattr(old_obj, fieldname):
                    change_flag = True
                    break
            if change_flag:
                self.pk = None
                max_revision = int(Article.objects.filter(slug=self.slug).aggregate(models.Max('revision'))['revision__max'])
                if self.revision <= max_revision:
                    self.revision = max_revision + 1

        with transaction.atomic():
            # render html with tag language
            template = Template(self.bleach_html())
            con = Context({})
            self.rendered = template.render(con)
            self.full_clean()  # WARNING extra validation
            if self.public:
                qs = Article.objects.filter(slug=self.slug)
                qs.update(public=False)
                super(Article, self).save(*args, **kwargs)
            else:
                super(Article, self).save(*args, **kwargs)

    def preprocess_images(self):
        ptrn = re.compile("\{\{ aimg_(?P<id>\d+) \}\}")
        return ptrn.sub(repl_image, self.html)

    def safe_html(self):
        return html.mark_safe(self.rendered)
